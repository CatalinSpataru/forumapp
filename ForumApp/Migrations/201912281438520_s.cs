namespace ForumApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class s : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Replies", "Topic_Id", "dbo.Topics");
            DropIndex("dbo.Replies", new[] { "Topic_Id" });
            DropColumn("dbo.Replies", "Topic_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Replies", "Topic_Id", c => c.Int());
            CreateIndex("dbo.Replies", "Topic_Id");
            AddForeignKey("dbo.Replies", "Topic_Id", "dbo.Topics", "Id");
        }
    }
}
